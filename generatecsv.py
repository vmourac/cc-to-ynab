import os
import sys
import argparse

from CSVFile import CSVFile
from banks.ItauParser import ItauParser


parser = argparse.ArgumentParser()
parser.add_argument("-i", required=True, help="Input file. ie: bankpage.htm")
parser.add_argument("-o", required=False, help="Optional. Desired output filename")

specified_args = parser.parse_args()
input_file = specified_args.i
output_file = specified_args.o

try:
    html = open(input_file).read()

    # If -o is not specified, the output filename will be generated automatically
    itau = None
    if output_file is None:
        itau = ItauParser(html, None)
        output_file = itau.parse_titular()
        csv = CSVFile(output_file)
        itau.csv_file = csv
    else:
        csv = CSVFile(output_file)
        itau = ItauParser(html, csv)

    itau.process()
    csv.close_file()

    curr_dir = os.path.dirname(__file__)
    print('Program executed sucessfully!' + os.linesep +
          'Output file location: ' + os.path.join(curr_dir, csv.file.name))
except Exception as e:
    tb = sys.exc_info()[2]
    print('Error executing program.')
    print(e.with_traceback(tb))
