import re
import itertools
from bs4 import BeautifulSoup
from exceptions.BankParserException import BankParserException


class ItauParser():
    """" ItauParser encapsula a lógica de negócio necessária para realizar
         o parse de um HTML exportado do banco Itau """

    regex_nome = r"^.*<!-- Dados Titular -->(.*)<!-- Box Resumo da Fatura -->.*$"

    regex_national = r"^.*<!-- Início Débitos e despesas nacionais -->" \
                     r"(.*)" \
                     r"<!-- Término Débitos e despesas nacionais -->.*$"

    regex_international = r"^.*<!-- Início Débitos e despesas internacionais -->" \
                          r"(.*)" \
                          r"<!-- Término Débitos e despesas internacionais -->.*$"

    def __init__(self, html, csv_file):
        self.html = html
        self.csv_file = csv_file

    def process(self):
        # Soup parsing and file writing
        self._parse_nacional()
        self._parse_internacional()

    def parse_titular(self):
        try:
            titular_entries = self._extract_regex(self.html, self.regex_nome)
            soup = BeautifulSoup(titular_entries)
            # Normally 2 tables are found. One is empty, and the other contains 2 rows
            # This ignores the empty list
            table = soup.find_all("table")[1]
            cols = table.find_all('td', {'class': ['TRNdado']})
            nome = cols[0].string.strip()
            cartao = cols[1].string.strip()
            return nome + ' - ' + cartao + '.csv'
        except:
            raise BankParserException('Error! Unexpected content found when retrieving output filename')

    def _parse_nacional(self):
        attr_map = {'class': ['TRNcampo_linha', 'TRNtitcampo_linha']}
        nat_entries = self._extract_regex(self.html, self.regex_national)
        soup = BeautifulSoup(nat_entries)
        tables = soup.find_all("table")  # Espera-se encontrar somente uma.
        for table in tables:
            rows = table.find_all('tr')
            for row in itertools.islice(rows, 3, len(rows)):
                self.process_row(self.csv_file, row, attr_map)

    def _parse_internacional(self):
        attr_map = {'class': ['TRNcampo', 'TRNdado', 'TRNtitcampo']}
        intl_entries = self._extract_regex(self.html, self.regex_international)
        soup = BeautifulSoup(intl_entries)
        tables = soup.find_all("table")
        for table in tables:
            rows = table.find_all('tr')
            for row in rows:
                self.process_row(self.csv_file, row, attr_map)

    @staticmethod
    def _extract_regex(html, regex):
        pattern = re.search(regex, html, re.DOTALL)
        if pattern:
            return pattern.group(1)
        else:
            raise BankParserException("Error. Regex pattern match returned None")

    @staticmethod
    def process_row(csv_file, row, attr_map):
        entry = row.find_all('td', attrs=attr_map)
        if entry is not None:
            if len(entry) == 3:
                date = entry[0].string
                payee = entry[1].string

                flow = entry[2].string.replace(',', '.')
                inflow = 0
                outflow = 0
                if "-" not in flow:
                    outflow = flow
                else:
                    inflow = flow.replace("-", "")
                csv_file.write_line(date, payee, outflow, inflow)
        else:
            raise BankParserException("Error. HTML parser was unable to find row content")