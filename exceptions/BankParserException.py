
class BankParserException(Exception):
	"""Indicates that the parser used in the bank HTML file has, somehow, failed"""

	def __init__(self, value):
		self.value = value

	def __str__(self):
		return repr(self.value)
		