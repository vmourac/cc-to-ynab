import os


class CSVFile:
    """Responsible for creating and manipulating a YNAB CSV file"""

    header = 'Date, Payee, Category, Memo, Outflow, Inflow'

    def __init__(self, path):
        self.file = self.create_file(path)

    def create_file(self, path):
        if os.path.exists(path):
            raise FileExistsError('Error! Specified file already exists.')

        self.file = open(path, "w")
        self.file.write(self.header + os.linesep)
        return self.file

    def write_line(self, date, payee, outflow, inflow):
        new_entry = str(date) + ',' + str(payee) + ',,,' + str(outflow) + "," + str(inflow)
        self.file.write(new_entry + os.linesep)

    def close_file(self):
        self.file.close()
